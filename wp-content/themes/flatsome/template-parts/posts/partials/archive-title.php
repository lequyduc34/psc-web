<header class="archive-page-header">
	<div class="row">
	<div class="large-12 text-center col">
	<section class="section has-block tooltipstered" id="section_381759064">
		<div class="bg section-bg fill bg-fill bg-loaded">

			
			
			

		</div>

		<div class="section-content relative">
			

	<div id="text-2209206139" class="text">
		

<h2 class="title-font" style="text-align: center; margin: 20px 0 10px;"><span style="color: #b20000; font-size: 40px;">TIN TỨC & SỰ KIỆN</span></h2>
		
<style>
#text-2209206139 {
  text-align: center;
  color: rgb(0, 0, 0);
}
#text-2209206139 > * {
  color: rgb(0, 0, 0);
}
</style>
	</div>
	
	<div id="text-2409359943" class="text">
		

<!--<p><span style="font-size: 120%;">Cập nhật những tin tức mới nhất về các dự án, thông tin và hoạt động của PSC.</span></p>-->
		
<style>
#text-2409359943 {
  text-align: center;
  color: rgb(0,0,0);
}
#text-2409359943 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
	

		</div>

		
<style>
#section_381759064 {
  padding-top: 0px;
  padding-bottom: 0px;
}
</style>
	</section>
	<?php
		if ( is_category() ) :
			// show an optional category description
			$category_description = category_description();
			if ( ! empty( $category_description ) ) :
				echo apply_filters( 'category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>' );
			endif;

		elseif ( is_tag() ) :
			// show an optional tag description
			$tag_description = tag_description();
			if ( ! empty( $tag_description ) ) :
				echo apply_filters( 'tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>' );
			endif;

		endif;
	?>
	</div>
	</div>
</header>