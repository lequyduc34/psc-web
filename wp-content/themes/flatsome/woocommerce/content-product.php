<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( fl_woocommerce_version_check( '4.4.0' ) ) {
	if ( empty( $product ) || false === wc_get_loop_product_visibility( $product->get_id() ) || ! $product->is_visible() ) {
		return;
	}
} else {
	if ( empty( $product ) || ! $product->is_visible() ) {
		return;
	}
}

// Check stock status.
$out_of_stock = ! $product->is_in_stock();

// Extra post classes.
$classes   = array();
$classes[] = 'product-small';
$classes[] = 'col';
$classes[] = 'has-hover';

if ( $out_of_stock ) $classes[] = 'out-of-stock';

?>

<div <?php wc_product_class( $classes, $product ); ?>>
	<div class="col-inner">
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	<div class="product-small box-custom-field box <?php echo flatsome_product_box_class(); ?>">
		<div class="box-image">
			<div class="<?php echo flatsome_product_box_image_class(); ?>">
				<a href="<?php echo get_the_permalink(); ?>">
					<?php
						/**
						 *
						 * @hooked woocommerce_get_alt_product_thumbnail - 11
						 * @hooked woocommerce_template_loop_product_thumbnail - 10
						 */
						do_action( 'flatsome_woocommerce_shop_loop_images' );
					?>
				</a>
			</div>
			<div class="image-tools is-small top right show-on-hover">
				<?php do_action( 'flatsome_product_box_tools_top' ); ?>
			</div>
			<div class="image-tools is-small hide-for-small bottom left show-on-hover">
				<?php do_action( 'flatsome_product_box_tools_bottom' ); ?>
			</div>
			<div class="image-tools <?php echo flatsome_product_box_actions_class(); ?>">
				<?php do_action( 'flatsome_product_box_actions' ); ?>
			</div>
			<?php if ( $out_of_stock ) { ?><div class="out-of-stock-label"><?php _e( 'Out of stock', 'woocommerce' ); ?></div><?php } ?>
		</div>
<div class="box-text text-center custom-f">
			<div class="box-text-inner">
				

	<div id="text-3372441594" class="text">
		<div class="box-text text-hr <?php echo flatsome_product_box_text_class(); ?>">
		<?php
				do_action( 'woocommerce_before_shop_loop_item_title' );

				echo '<div class="title-wrapper"><h2 class="title-font" style="font-size:25px;">';
				do_action( 'woocommerce_shop_loop_item_title' );
				echo '</h2></div>';


				echo '<div class="price-wrapper">';
				do_action( 'woocommerce_after_shop_loop_item_title' );
				echo '</div>';

				do_action( 'flatsome_product_box_after' );
               ?>

<hr >
		<div class="col-inner">
	<div id="text-3688912439" class="text" style="font-size:20px;">
<p><?php the_field('dia_chi') ?></p>	
<style>
#text-3688912439 {
  color: rgb(0,0,0);
}
#text-3688912439 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
<div class="row" id="row-1665155256">
	<div id="col-54018080" class="col medium-8 small-8 large-8 pd-custom">
		<div class="col-inner">
	<div id="text-3350154405" class="text">
<p><strong><?php the_field('loai_hinh_bdss') ?></strong></p>
<style>
#text-3350154405 {
  color: rgb(0,0,0);
}
#text-3350154405 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
		</div>
			</div>
	<div id="col-1545976027" class="col medium-4 small-4 large-4 pd-custom">
		<div class="col-inner text-right">
	<div id="text-1099518807" class="text">
<p><strong><?php the_field('dien_tich') ?></strong></p>	
<style>
#text-1099518807 {
  color: rgb(0,0,0);
}
#text-1099518807 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
		</div>
			</div>
</div>
		</div>
			</div>
		
<style>
#text-3372441594 {
  text-align: left;
  color: rgb(0,0,0);
}
#text-3372441594 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
	

			</div>
		</div>
       

		</div>

	<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	</div>
</div>