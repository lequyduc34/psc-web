<div class="product-container">
  <div class="product-main">
    <div class="row content-row mb-0">

<section class="section" id="section_1651623057">
		<div class="bg section-bg fill bg-fill bg-loaded">
		</div>
		<div class="section-content relative">
			<div class="row" id="row-199141475">
				<div id="col-1600860778" class="col small-12 large-12">
					<div class="col-inner text-center">
						<div id="text-1512346190" class="text">
							<?php
    			/**
    			 * woocommerce_single_product_summary hook
    			 *
    			 * @hooked woocommerce_template_single_title - 5
    			 * @hooked woocommerce_template_single_rating - 10
    			 * @hooked woocommerce_template_single_price - 10
    			 * @hooked woocommerce_template_single_excerpt - 20
    			 * @hooked woocommerce_template_single_add_to_cart - 30
    			 * @hooked woocommerce_template_single_meta - 40
    			 * @hooked woocommerce_template_single_sharing - 50
    			 */
    			do_action( 'woocommerce_single_product_summary' );
    		?>
		
                            <style>
                            #text-1512346190 {
                              text-align: center;
                              color: rgb(0,0,0);
                            }
                            #text-1512346190 > * {
                              color: rgb(0,0,0);
                            }
                            </style>
                        </div>
						<div class="text-center"><div class="is-divider divider clearfix" style="margin-top:1em;margin-bottom:0.3em;max-width:275px;height:4px;background-color:rgb(208, 31, 37);"></div></div>
						<div id="text-1763939631" class="text" style="margin-bottom:20px;">
		
							<h3 ><span style="color: #d01f25;font-family:psc2;"><strong><?php the_field("short_title") ?></strong></span></h3>
		
                            <style>
                            #text-1763939631 {
                              text-align: center;
                            }
                            </style>
						</div>
	
<!--                        <a href="tel:0936055030" target="_self" class="button alert lowercase">-->
<!--                            <span style="font-size:16px;">TƯ VẤN 24/7: 0936 055 030</span>-->
<!--                          </a>-->

                        <a href="/lien-he/" class="button alert is-outline lowercase">
                            <span style="font-size:16px;">LIÊN HỆ ĐẶT LỊCH</span>
                          </a>

					</div>
				</div>	
			</div>
            <div class="row" id="row-1167581788">

	<div id="col-209183209" class="col medium-6 small-12 large-6 tongquan">
		<div class="col-inner add-font" >
			
			
<?php the_field("tong_quan_1") ?>
		</div>
			</div>

	

	<div id="col-187784998" class="col medium-6 small-12 large-6 tongquan">
		<div class="col-inner add-font" >
			
			
<?php the_field("tong_quan_2") ?>
		</div>
			</div>

	

	<div id="col-1451896810" class="col medium-6 small-12 large-6">
		<div class="col-inner">
			
			
	<div class="box has-hover   has-hover box-text-bottom">

                    <div class="box gallery-box dark has-hover box-text-bottom">
		<div class="box-image">
						<div class="">
                        <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_thiet_ke_2d") ?>" >
				<img width="800" height="600"
                                    src="<?php the_field("anh_thiet_ke_2d") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_thiet_ke_2d") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">	 </a>										
					</div>
                    </div>

		<div class="box-text text-center">
			<div class="box-text-inner">
				
<a href="<?php the_field("anh_thiet_ke_2d") ?>" target="_self" class="button alert is-large lowercase image-lightbox lightbox-gallery" >
    <span style="font-size:18px;">Xem ảnh mặt bằng</span>
  </a>

			</div>
		</div>
	</div>
    </div>
   
		</div>
			</div>

	

	<div id="col-613347673" class="col medium-6 small-12 large-6">
		<div class="col-inner">
			
			
	<div class="box has-hover   has-hover box-text-bottom">

		<div class="box-image">
						<div class="">
                        <a href="<?php the_field("link_tai_tai_lieu") ?>" target="_blank">
				<img width="800" height="600"
                                    src="<?php the_field("anh_tai_lieu") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_tai_lieu") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">		
                                    </a>
                                    </div>
					</div>

		<div class="box-text text-center">
			<div class="box-text-inner">
				
<a href="<?php the_field("link_tai_tai_lieu") ?>" target="_blank" class="button is-large alert lowercase">
    <span style="font-size:18px;">Tải tài liệu</span>
  </a>

			</div>
		</div>
	</div>
	
		</div>
			</div>

	
</div>

		</div>

		
<style>
#section_1651623057 {
  padding-top: 30px;
  padding-bottom: 30px;
}
</style>
	</section>
    </div>
  </div>

  
</div>

<iframe src="<?php the_field("ban_do") ?>" width="1920" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
<?php echo do_shortcode ('[block id="bottomshop"]') ?>