<div class="page-title shop-page-title product-page-title">
	<div class="page-title-inner flex-row medium-flex-wrap container">
	  <div class="flex-col flex-grow medium-text-center">
	  		<?php do_action('flatsome_product_title') ;?>
	  </div>
	  
	   <div class="flex-col medium-text-center">
		   	<?php do_action('flatsome_product_title_tools') ;?>
	   </div>
	</div>
</div>

<section class="section" id="section_419175798">
		<div class="bg section-bg fill bg-fill bg-loaded">

			
			<div class="section-bg-overlay absolute fill"></div>
			

		</div>

		<div class="section-content relative">
			
<div class="row" id="row-2062856520">

	<div id="col-1339657440" class="col small-12 large-12">
		<div class="col-inner">
			
			
	<div id="text-1710280619" class="text">
		
<?php
    			/**
    			 * woocommerce_single_product_summary hook
    			 *
    			 * @hooked woocommerce_template_single_title - 5
    			 * @hooked woocommerce_template_single_rating - 10
    			 * @hooked woocommerce_template_single_price - 10
    			 * @hooked woocommerce_template_single_excerpt - 20
    			 * @hooked woocommerce_template_single_add_to_cart - 30
    			 * @hooked woocommerce_template_single_meta - 40
    			 * @hooked woocommerce_template_single_sharing - 50
    			 */
    			do_action( 'woocommerce_single_product_summary' );
    		?>
		
<style>
#text-1710280619 {
  text-align: center;
  color: rgb(255,255,255);
}
#text-1710280619 > * {
  color: rgb(255,255,255);
}
</style>
	</div>
	
<div class="text-center"><div class="is-divider divider clearfix" style="margin-top:0.1em;margin-bottom:0.1em;max-width:275px;height:4px;background-color:rgb(208, 31, 37);"></div></div>
	<div id="text-2623727635" class="text">
		
<p style="font-size:23px;"><strong><?php the_field("loai_hinh_bdss") ?></strong></p>
		
<style>
#text-2623727635 {
  text-align: center;
  color: rgb(255,255,255);
}
#text-2623727635 > * {
  color: rgb(255,255,255);
}
</style>
	</div>
	
		</div>
			</div>

	
</div>
		</div>

		
<style>
#section_419175798 {
  padding-top: 200px;
  padding-bottom: 200px;
}
#section_419175798 .section-bg-overlay {
  background-color: rgba(0, 0, 0, 0.458);
}
#section_419175798 .section-bg.bg-loaded {
  background-image: url(<?php the_field("banner") ?>);
}
#section_419175798 .section-bg {
  background-position: 28% 53%;
}
</style>
	</section>
    
    <section class="section home-gioi-thieu-2" id="section_1985828960">
		<div class="bg section-bg fill bg-fill bg-loaded">

		</div>

		<div class="section-content relative">
			
<div class="row" id="row-920375277">

	<div id="col-1402660385" class="col medium-6 small-12 large-6">
		<div class="col-inner text-left">
			
			
	<div id="text-1998059178" class="text">
		
<h4 class="uppercase" style="color: #cf2127;"><?php the_field("dia_chi") ?></h4>
<p><span style="color: #000000;font-size:22px;font-family:roboto;"><strong><?php the_field("mo_ta_ngan") ?></strong></span></p>
		
<style>
#text-1998059178 {
  text-align: left;
}
</style>
	</div>
	
<a class="button is-larger alert lowercase" href='tel:0936055030'>
    <span style="font-size:17.5px">TƯ VẤN  24/7: 0936 055 030</span>
  </a>

<a class="button is-larger alert is-outline lowercase" href="/lien-he">
    <span style="font-size:17.5px">ĐẶT LỊCH XEM MẶT BẰNG</span>
  </a>

		</div>
			</div>


	<div id="col-310318428" class="col medium-6 small-12 large-6">
		<div class="col-inner">
			
			
<div class="video video-fit mb" style="padding-top:56.25%;"><p><iframe loading="lazy"  width="1020" height="574" src="<?php the_field("video") ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe></p>
</div>
		</div>
			</div>

	
</div>
		</div>

		
<style>
#section_1985828960 {
  padding-top: 30px;
  padding-bottom: 30px;
}
</style>

	</section>
<section class="section" >
<div class="section-content relative">
<div class="row" style="max-width:1540px">
<div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36075055">
			<a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_chinh") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
	
								<div class="img-inner dark">
							<img width="1000" height="650"
                                    src="<?php the_field("anh_chinh") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_chinh") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">					
					</div>
								
<style>
#image_36075055 {
  width: 100%;
}
</style>
	
	</div>
    </a>
    </div>
    </div>
    </div>
</section>
<section class="section" id="section_3336129">
		<div class="bg section-bg fill bg-fill bg-loaded">

		</div>

		<div class="section-content relative">
			
<div class="row" id="row-1168622701">

	<div id="col-1470645392" class="col small-12 medium-12 large-12">
		<div class="col-inner">
			
	<div id="text-434444272" class="text">
		
<?php the_field("gioi_thieu_chung") ?>
		
<style>
#text-434444272 {
  text-align: left;
  color: rgb(0,0,0);
  font-weight:bold;
}
#text-434444272 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
	
		</div>
			</div>

	
</div>
		</div>

		
<style>
#section_3336129 {
  padding-top: 0px;
  padding-bottom: 30px;
}
#section_3336139 {
  padding-top: 0px;
  padding-bottom: 0px;
}
</style>
	</section>
    	<section class="section" id="section_3336139">
<div class="section-content relative radius-img">
        <div class="row" id="row-1053550603">
            <div id="col-844263428" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                     <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_1") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_1") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_1") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                        <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-1464847442" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_1") ?></p>
                                    <style>
                                        #text-1464847442 {
                                            color: rgb(0, 0, 0);
                                        }

                                        #text-1464847442>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <style>
                    #col-844263428>.col-inner {
                        margin: 0px 0px -10px 0px;
                    }
                </style>
            </div>
            <div id="col-497000855" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                     <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_2") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_2") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_2") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                        <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-2275719340" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_2") ?></p>
                                    <style>
                                        #text-2275719340 {
                                            color: rgb(0, 0, 0);
                                        }
                                        #text-2275719340>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <style>
                    #col-497000855>.col-inner {
                        margin: 0px 0px -10px 0px;
                    }
                </style>
            </div>
            <div id="col-1637770736" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_3") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_3") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_3") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                       <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-30465035" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_3") ?></p>
                                    <style>
                                        #text-30465035 {
                                            color: rgb(0, 0, 0);
                                        }

                                        #text-30465035>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                       </div>
                       </a>
                    </div>
                    </div>
                </div>
                <div class="row" id="row-1053550603">
            <div id="col-844263428" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                     <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_4") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_4") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_4") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                        <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-1464847442" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_4") ?></p>
                                    <style>
                                        #text-1464847442 {
                                            color: rgb(0, 0, 0);
                                        }

                                        #text-1464847442>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <style>
                    #col-844263428>.col-inner {
                        margin: 0px 0px -10px 0px;
                    }
                </style>
            </div>
            <div id="col-497000855" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                     <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_5") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_5") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_5") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                        <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-2275719340" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_5") ?></p>
                                    <style>
                                        #text-2275719340 {
                                            color: rgb(0, 0, 0);
                                        }
                                        #text-2275719340>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
                <style>
                    #col-497000855>.col-inner {
                        margin: 0px 0px -10px 0px;
                    }
                </style>
            </div>
            <div id="col-1637770736" class="col medium-4 small-12 large-4">
                <div class="col-inner">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_usp_6") ?>" title="">
                    <div class="box gallery-box dark has-hover box-text-bottom">
                        <div class="box-image">
                            <div class="">
                                <img width="800" height="450"
                                    src="<?php the_field("anh_usp_6") ?>"
                                    class="attachment- size-" alt="" loading="lazy"
                                    srcset="<?php the_field("anh_usp_6") ?>"
                                    sizes="(max-width: 800px) 100vw, 800px">
                            </div>
                        </div>
                       <div class="box-text text-center">
                            <div class="box-text-inner">
                                <div id="text-30465035" class="text text-bolder">
                                    <p style="font-size: 21px;"><?php the_field("mo_ta_usp_6") ?></p>
                                    <style>
                                        #text-30465035 {
                                            color: rgb(0, 0, 0);
                                        }

                                        #text-30465035>* {
                                            color: rgb(0, 0, 0);
                                        }
                                    </style>
                                </div>
                            </div>
                        </div>
                       </div>
                       </a>
                    </div>
                    </div>
                    <div id="col-1470645392" class="col small-12 medium-12 large-12">
		<div class="col-inner">
			
	<div id="text-434444272" class="text">
		
<?php the_field("mo_ta_chung_2") ?>
		
<style>
#text-434444272 {
  text-align: left;
  color: rgb(0,0,0);
  font-size:20px;
}
#text-434444272 > * {
  color: rgb(0,0,0);
}
</style>
	</div>
	
		</div>
			</div>
</section>
<?php if(get_field('anh_bo_sung_1')) { ?>
    <section class="section" style="padding-bottom:0px;">
    <div class="section-content relative">
    <div class="row" style="max-width:1540px">
    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36075055">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_bo_sung_1") ?>" title="">
                        <div class="box gallery-box dark has-hover box-text-bottom">

                                    <div class="img-inner dark">
                                <img width="1000" height="650"
                                        src="<?php the_field("anh_bo_sung_1") ?>"
                                        class="attachment- size-" alt="" loading="lazy"
                                        srcset="<?php the_field("anh_bo_sung_1") ?>"
                                        sizes="(max-width: 800px) 100vw, 800px">					
                        </div>

    <style>
    #image_36075055 {
      width: 100%;
    }
    </style>

        </div>
        </a>
        </div>
        </div>
        </div>
    </section>
<?php }?>
<?php if(get_field('anh_bo_sung_2')) { ?>
    <section class="section" style="padding-bottom:0px;">
    <div class="section-content relative">
    <div class="row" style="max-width:1540px">
    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36075055">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_bo_sung_2") ?>" title="">
                        <div class="box gallery-box dark has-hover box-text-bottom">

                                    <div class="img-inner dark">
                                <img width="1000" height="650"
                                        src="<?php the_field("anh_bo_sung_2") ?>"
                                        class="attachment- size-" alt="" loading="lazy"
                                        srcset="<?php the_field("anh_bo_sung_2") ?>"
                                        sizes="(max-width: 800px) 100vw, 800px">					
                        </div>

    <style>
    #image_36075055 {
      width: 100%;
    }
    </style>

        </div>
        </a>
        </div>
        </div>
        </div>
    </section>
<?php }?>
<?php if(get_field('anh_bo_sung_3')) { ?>
    <section class="section" style="padding-bottom:0px;">
    <div class="section-content relative">
    <div class="row" style="max-width:1540px">
    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36075055">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_bo_sung_3") ?>" title="">
                        <div class="box gallery-box dark has-hover box-text-bottom">

                                    <div class="img-inner dark">
                                <img width="1000" height="650"
                                        src="<?php the_field("anh_bo_sung_3") ?>"
                                        class="attachment- size-" alt="" loading="lazy"
                                        srcset="<?php the_field("anh_bo_sung_3") ?>"
                                        sizes="(max-width: 800px) 100vw, 800px">					
                        </div>

    <style>
    #image_36075055 {
      width: 100%;
    }
    </style>

        </div>
        </a>
        </div>
        </div>
        </div>
    </section>
<?php }?>
<?php if(get_field('anh_bo_sung_4')) { ?>
    <section class="section" style="padding-bottom:0px;">
    <div class="section-content relative">
    <div class="row" style="max-width:1540px">
    <div class="img has-hover x md-x lg-x y md-y lg-y" id="image_36075055">
                <a class="image-lightbox lightbox-gallery" href="<?php the_field("anh_bo_sung_4") ?>" title="">
                        <div class="box gallery-box dark has-hover box-text-bottom">

                                    <div class="img-inner dark">
                                <img width="1000" height="650"
                                        src="<?php the_field("anh_bo_sung_4") ?>"
                                        class="attachment- size-" alt="" loading="lazy"
                                        srcset="<?php the_field("anh_bo_sung_4") ?>"
                                        sizes="(max-width: 800px) 100vw, 800px">					
                        </div>

    <style>
    #image_36075055 {
      width: 100%;
    }
    </style>

        </div>
        </a>
        </div>
        </div>
        </div>
    </section>
<?php }?>