<?php
/**
 * Quote List Page
 *
 * This list of Quotes for the Portal
 *
 * @author 		ZeroBSCRM
 * @package 	Templates/Portal/Quotes
 * @see			https://jetpackcrm.com/kb/
 * @version     3.0
 * 
 */



if ( ! defined( 'ABSPATH' ) ) exit; // Don't allow direct access

$ZBSuseQuotes = zeroBSCRM_getSetting('feat_quotes');
//zeroBS_portal_enqueue_stuff();

do_action( 'zbs_enqueue_scripts_and_styles' );

$portalLink = zeroBS_portal_link();

if($ZBSuseQuotes < 0){
        status_header( 404 );
        nocache_headers();
        include( get_query_template( '404' ) );
        die();
}

add_action( 'wp_enqueue_scripts', 'zeroBS_portal_enqueue_stuff' );
?>
<div class="zbs-site-main zbs-portal-grid">
    <nav class="zbs-portal-nav">
    <?php
        //moved into func
        if(function_exists('zeroBSCRM_clientPortalgetEndpoint')){
            $quote_endpoint = zeroBSCRM_clientPortalgetEndpoint('quotes');
        }else{
            $quote_endpoint = 'quotes';
        }
        zeroBS_portalnav($quote_endpoint);
    ?>
    </nav>


    <div class="zbs-portal-content">
<?php
	global $wpdb;
	$uid = get_current_user_id();
	$uinfo = get_userdata( $uid );
	$cID = zeroBS_getCustomerIDWithEmail($uinfo->user_email);
	$currencyChar = zeroBSCRM_getCurrencyChr();

	// preview msg		
	zeroBSCRM_portal_adminPreviewMsg($cID,'margin-bottom:1em');

	// admin msg (upsell cpp) (checks perms itself, safe to run)
	zeroBSCRM_portal_adminMsg();

	$customer_quotes = zeroBS_getQuotesForCustomer($cID,true,100,0,false);

    ?><h2><?php _e('Quotes','zero-bs-crm'); ?></h2>
            <div class='zbs-entry-content zbs-responsive-table' style="position:relative;">
                <?php

	if(count($customer_quotes) > 0){
		echo '<table class="table">';
		echo '<thead>';
		echo '<th>' . __('#',"zero-bs-crm") . '</th>';
		echo '<th>' . __('Date',"zero-bs-crm") . '</th>';
		echo '<th>' . __('Title',"zero-bs-crm") . '</th>';
		echo '<th>' . __('Total',"zero-bs-crm") . '</th>';
		echo '<th>' . __('Status',"zero-bs-crm") . '</th>';
		// echo '<th>' . __('Download PDF',"zero-bs-crm") . '</th>';
        echo '</thead>';

		foreach($customer_quotes as $cquo){
			// Quote Date
			$quote_date = __("No date", "zero-bs-crm");
			if (isset($cquo['date_date']) && !empty($cquo['date_date'])) {
				$quote_date = $cquo['date_date'];
			}

			// Quote Status
			$quote_stat = zeroBS_getQuoteStatus($cquo);

			// Quote Value
			$quote_value = '';
			if (isset($cquo['value']) && !empty($cquo['value'])){
				$quote_value = zeroBSCRM_formatCurrency($cquo['value']);
			}
		
        	// view on portal (hashed?)
        	$quote_url = zeroBSCRM_portal_linkObj($cquo['id'],ZBS_TYPE_QUOTE);

			// Quote Title
			// Default value is set to '&nbsp;' to force rendering the cell. The css "empty-cells: show;" doesn't work in this type of table
			$quote_title = '&nbsp;';
			if (isset($cquo['title']) && !empty($cquo['title'])){
				$quote_title = $cquo['title'];
			}

			echo '<tr>';
			echo '<td data-title="' . __('#',"zero-bs-crm") . '"><a href="'. $quote_url .'">#'. $cquo['id'] .' '. __('(view)','zero-bs-crm') . '</a></td>';
				echo '<td data-title="' . __('Date',"zero-bs-crm") . '">' . $quote_date . '</td>';
				echo '<td data-title="' . __('Title',"zero-bs-crm") . '"><span class="name">'.$quote_title.'</span></td>';
				echo '<td data-title="' . __('Total',"zero-bs-crm") . '">' . $quote_value . '</td>';
				echo '<td data-title="' . __('Status',"zero-bs-crm") . '"><span class="status">'.$quote_stat.'</span></td>';
			echo '</tr>';
		}
		echo '</table>';
	}else{
		_e('You do not have any quotes yet.',"zero-bs-crm"); 
	}
	?>
        </div>
    </div>
    <div class="zbs-portal-grid-footer"><?php zeroBSCRM_portalFooter(); ?></div>
</div>
